#!/bin/bash
clear
if [ "$EUID" -ne 0 ]
	then echo "Must be root. Abort!"
	exit
fi

echo -e "######################################"
echo -e "##### Setup RPi via raspi-config #####"
echo -e "######################################"

# setup locale to US
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US.UTF-8
dpkg-reconfigure locales

# disable splash screen
raspi-config nonint do_boot_splash 0

# expand filesystem
raspi-config nonint do_expand_rootfs

# set gpu memory to 256
raspi-config nonint get_config_var gpu_mem_256 /boot/config.txt

# set boot to CLI with autologin
sudo raspi-config nonint do_boot_behaviour B2

echo -e "#####################################"
echo -e "##### Update and upgrade system #####"
echo -e "#####################################"
sleep 3
apt-get update && apt-get upgrade -y
clear

echo -e "############################################"
echo -e "##### Make playout folder and chown it #####"
echo -e "############################################"
sleep 3
mkdir /home/pi/playout/
chown pi:pi /home/pi/playout/
clear

echo -e "###############################"
echo -e "##### Copy files in place #####"
echo -e "###############################"
sleep 3

# test file
cp -a timecodetest.mp4 /home/pi/playout/timecodetest.mp4

# omxplayer-sync configs
cp -a configfile.txt /boot/configfile.txt

#enable ssh
cp -a ssh /boot/ssh

# wifi
cp -a wpa_supplicant.conf /boot/wpa_supplicant.conf

# playout script
cp -a playvideos.sh /home/pi/playvideos.sh
chmod +x /home/pi/playvideos.sh
# sed -i -e '$i \sudo /home/pi/playvideos.sh &\n' /etc/rc.local
cp rpi-omxsync.service.txt /lib/systemd/system/rpi-omxsync.service
chmod 644 /lib/systemd/system/rpi-omxsync.service
systemctl daemon-reload
systemctl enable rpi-omxsync.service
clear
# systemctl start rpi-omxsync.service
# systemctl status rpi-omxsync.service
# sleep 5

echo -e "####################################"
echo -e "##### RPi hostname? eq. rpi-v2 #####"
echo -e "####################################"
read hostnameinput
raspi-config nonint do_hostname $hostnameinput

# network (eth0)
mv /etc/dhcpcd.conf /etc/dhcpcd.conf.original
cp -a dhcpcd.conf /boot/dhcpcd.conf
ln -s /boot/dhcpcd.conf /etc/dhcpcd.conf
clear

echo -e "#############################################"
echo -e "##### Intall shutdown button/led script #####"
echo -e "#############################################"
sleep 3
cp shutdown-monitor.py /usr/local/bin/shutdown-monitor.py
chmod 700 /usr/local/bin/shutdown-monitor.py
sed -i -e '$i /usr/bin/python /usr/local/bin/shutdown-monitor.py  > /dev/null 2>&1 &'  /etc/rc.local
clear

echo -e "################################################################"
echo -e "##### Remove defualt OMXplayer and install omxplayer_0.3.7 #####"
echo -e "################################################################"
sleep 3
apt-get remove -y omxplayer
rm -rf /usr/bin/omxplayer /usr/bin/omxplayer.bin /usr/lib/omxplayer

apt-get install -y libpcre3 fonts-freefont-ttf fbset libssh-4 python3-dbus exfat-fuse exfat-utils libsmbclient

#wget http://omxplayer.sconde.net/builds/omxplayer_0.3.7~git20160713~66f9076_armhf.deb
dpkg -i omxplayer_0.3.7~git20160713~66f9076_armhf.deb
apt-get -f install
dpkg -i omxplayer_0.3.7~git20160713~66f9076_armhf.deb
apt-get update
apt-get -f install -y

#wget -O /usr/bin/omxplayer-sync https://github.com/HsienYu/omxplayer-sync/raw/master/omxplayer-sync
cp -a omxplayer-sync /usr/bin/omxplayer-sync
chmod 0755 /usr/bin/omxplayer-sync

#setup usb mount point and setup /etc/fstab
mkdir /media/USB
chmod a+r /media/USB
echo "# /dev/sda1	/media/USB	vfat	defaults	0	2" >> /etc/fstab

# disable screen blanking
# sed -i -e '$i \setterm -blank 1\n' /etc/rc.local


clear

echo -e "Note: USB drive must be MS-DOS(FAT32) aka vfat"
echo -e "To use USB drive as source change usb=0 to usb=1 in /boot/configfile.txt and unncomment last line (/dev/sda1	/media/USB...) in /etc/fstab"
sleep 1
echo -e ""
echo -e "Don't forget to change /boot/configfile.txt accordingly to your setup"
sleep 1
echo -e ""
echo -e "Set static IP with: chmod +x set_static_ip.sh && sudo ./set_static_ip.sh"
echo -e "You can revert changes removing static ip section in: /boot/dhcpcd.conf "
sleep 1
echo -e ""
echo -e "####################################"
echo -e "##### Done! Please reboot now! #####"
echo -e "####################################"
echo -e ""
echo -e ""
echo -e ""
echo -e "##############################"
echo -e "##############################"
echo -e "##############################"
echo -e "##############################"
echo -e "########## R.T.F.M. ##########"
echo -e "##############################"
echo -e "##############################"
echo -e "##############################"
echo -e "##############################"
echo -e ""
echo -e ""
echo -e ""
ip a | grep eth0
exit 0
