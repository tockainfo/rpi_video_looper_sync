#!/bin/bash
clear

cho -e "##################################"
echo -e "##### Changing eth0 to DCHP #####"
echo -e "#################################"
rm -rf /boot/dhcpcd.conf
cp -a dhcpcd.conf /boot/dhcpcd.conf
sleep 1
echo -e "####################################"
echo -e "##### Done, please reboot now! #####"
echo -e "####################################"
