#!/bin/bash
clear

echo "RPi IP address with range, eq. 192.168.0.150/24"
read staticIP
echo "# Static IP" >> /boot/dhcpcd.conf
echo "interface eth0" >> /boot/dhcpcd.conf
echo "static ip_address=$staticIP" >> /boot/dhcpcd.conf
echo ""
echo "write $staticIP to /boot/dhcpcd.conf"
echo ""
echo ""
sleep 1

echo "Gateway IP address, eq. 192.168.0.254"
read gatewayIP
echo "static routers=$gatewayIP" >> /boot/dhcpcd.conf
echo ""
echo "write $gatewayIP to /boot/dhcpcd.conf"
echo ""
echo ""
sleep 1

echo "DNS server IP, eq. 8.8.8.8"
read dnsIP
echo "static domain_name_servers=$dnsIP" >> /boot/dhcpcd.conf
echo ""
echo "write $dnsIP to /boot/dhcpcd.conf"
echo ""
echo ""
sleep 1
clear

echo "RPi IP: $staticIP"
echo "Gateway IP: $gatewayIP"
echo "DNS IP: $dnsIP"
echo ""
echo "now you can use SSH to enter RPi, ssh pi@$staticIP"
sleep 1
