# RPi_video_looper_sync
Player multiple video in sync and loop by OMXPlayer-sync w/ power button/led and auto boot to player

Setup:
-----
in CLI type:
```
$ cd ~/
$ git clone https://wisemediahr@bitbucket.org/wisemediahr/rpi_video_looper_sync.git
$ cd RPi_video_looper_sync
$ chmod +x install.sh
$ sudo su
# bash install.sh
```
and follow the install

connect all RPi's to the same network (lan is recommended)
WIFI setup in /boot/wpa_supplicant.conf

format your USB drive to vfat (MSDOS FAT32) format

To use usb auto-mount uncomment line in /etc/fstab/
```
# /dev/sda1	/media/USB	vfat	defaults	0	2
```

If you want to use ExFAT format usb drive to ExFAT and change in /etc/fstab line:
```
/dev/sda1	/media/USB	vfat	defaults	0	2
```
to:
```
/dev/sda1	/media/USB	exfat	defaults	0	2
```

Usage notes:
-----
 * Put files into /home/pi/playout/ folder
 * There is timecodetest.mp4 file added on install to playout folder for test
 * Make sure there are no other files than movie files (e.g. no pictures, no textfiles) in the folder where the movie is, otherwise you may get sync errors.
 * A RJ45 cable must be connected before you start the master, otherwise it will not send sync data to slave.
 * Use videos which are min. 60 seconds or longer
 * Prepare your video files in QT h.264 bit lower bitrate, 25fps
 * Files must have the same filename across all RPi's and same durations (same number of frames)
 * support H264 codec in .mp4 .avi .mkv .mov .mpg .m4v and mp3 audio

 * Note: I have noticed that after 2-3 loops all RPi's are synced (audio/video/timecode)


Configuration:
-----
edit /boot/configfile.txt

```
role=m --master device (can be used as single channel video loop player)
     l --slaver device
audio_source=hdmi or local or both
usb=1 --play video from USB media
    0 --play video from /home/pi/playout/
```

Static IP configuration:
-----
edit /boot/dhcpcd.conf and add to bottom

```
# Static IP
interface eth0
static ip_address=192.168.0.150/24
static routers=192.168.0.254
static domain_name_servers=192.168.0.254
```
and replace IP addresses to your network addresses

or, chmod +x set_static_ip.sh and then run it as root and follow wizard

Power/reboot button + LED:
-----
 * short press - sends RPi to reboot
 * long press - send RPi to shutdown
 * blinks every 5 sec as running indicator

Schema:

![RPi power button](https://www.marcelpost.com/wiki/images/5/58/Rpi-shutdown-button-schematic.png)

Parts:
 * 1x momentary button
 * 2x 1k resistor
 * 1x LED
 * some jumper cables

Wiring to RPi:
 * Connect GND to GND on RPi
 * Connect LED to GPIO11
 * Connect BTN to GPIO09
 ![RPi power button](https://www.marcelpost.com/wiki/images/2/27/Rpi-shutdown-button-text.jpg)
 * RTFM https://learn.sparkfun.com/tutorials/raspberry-gpio/gpio-pinout

Requirements:
-----
 * A recent version of Python3 or Python2.
 * A recent version of the [python bindings for D-Bus](http://www.freedesktop.org/wiki/Software/DBusBindings).  
 * A recent build of omxplayer from [Sergio Conde](http://omxplayer.sconde.net).

How to kill autorun of PWR btn script and video player script?
-----
edit /etc/rc.local with sudo

for power button, remove line:
```
/usr/bin/python /usr/local/bin/shutdown-monitor.py  > /dev/null 2>&1 &
```

for video player:
```
sudo service rpi-omxsync status|start|stop
```

How to install from null:
-------------------------
 * download latest Rasbian Stretch Lite (no GUI) from https://www.raspberrypi.org/downloads/raspbian/
 * flash img on SD card with Etcher https://www.balena.io/etcher/
 * after successful flash, reinsert SD card and on 'boot' partition add empty file named 'ssh' - this will enable you ssh server on first boot
 * after boot, login with uss: 'pi' and pass: 'raspberry'
 * type in CLI: sudo 'sudo raspi-config' and select '1. Change user password', after hit Finish
 * type in CLI: sudo apt-get install -y git
 * after git is installed, clone repo with git 'git clone https://wisemediahr@bitbucket.org/wisemediahr/rpi_video_looper_sync.git'
 * cd rpi_video_looper_sync
 * type in CLI: chmod +x install.sh
 * type in CLI: chmod +x set_static_ip.sh
 * install synclooper, powerbutton and all tweaks with 'bash install.sh' or './install.hr'
 * optionaly set static IP with 'bash set_static_ip.sh' or './set_static_ip.sh' if you dont have DHCP on network
 * reboot and viola, test video should be playing after reboot

Tested with:
 -----
 * 2x RPi 2 B, Rasbian Jessie, 1080p hdmi output, analog audio output, gigabit switch
 * 3x RPi 3 B+ Rasbian Strech, 1080p hdmi output, analog audio output, gigabit switch

Credits:
-------
 * omxplayer-sync https://github.com/turingmachine/
 * RPi_SyncLooper https://github.com/HsienYu/RPi_SyncLooper/
 * RPi PWR button https://www.marcelpost.com/
